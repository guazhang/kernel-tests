#!/bin/bash
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#  Copyright Red Hat, Inc
#
#  SPDX-License-Identifier: GPL-2.0-or-later
#
# This script validates the kernel maximum stack size using tracer.
#
# First it checks if the stack tracer is enabled and exits if not.
#
# If enabled it checks the current stack_max_size value, and compares it against a predefined threshold.
#
# If the stack_max_size exceeds the threshold, a warning is logged, and the
# function responsible for the maximum stack usage is identified.
#
# Inputs:
#   /sys/kernel/tracing/stack_max_size
#   /sys/kernel/tracing/stack_trace
#   /proc/sys/kernel/stack_tracer_enabled
#
# Expected resluts:
#   [   INFO   ] :: stack_max_size (<size> bytes) is within the acceptable range.
#
# Signed-off-by: Li Wang <liwang@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Define the threshold value (e.g., 13kB in bytes)
THRESHOLD=${THRESHOLD:-13312}

# ---------- Start Test -------------
rlJournalStart

rlPhaseStart "WARN" "Check if tracer was disabled"
    stack_results=${TMT_PLAN_DATA}/stack_results.log
    if [[ -f ${stack_results} ]]; then
        if ! rlAssertNotGrep "RESULTS: WARN" ${stack_results}; then
            rlFail "stack tracer was not enabled."
        fi
    else
        rlPass "Stack trace was enabled and no test failures."
        rlPhaseEnd
        rlJournalEnd
        exit 0
    fi
rlPhaseEnd

rlPhaseStartTest "Check if stack size exceeded threshold"
    if [[ -f ${stack_results} ]]; then
        if ! rlAssertNotGrep "RESULTS: FAIL" ${stack_results}; then
            rlFail "stack_max_size exceeds the threshold ($THRESHOLD bytes)."
        fi
    else
        rlPass "Stack trace was enabled and no test failures."
    fi
rlPhaseEnd

rlJournalPrintText
rlJournalEnd
