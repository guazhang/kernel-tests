#!/usr/bin/python
# Copyright (c) 2014 Red Hat, Inc. All rights reserved. This copyrighted material 
# is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General
# Public License v.2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Author: Ken Benoit

"""
Constant messages to be used during test execution.

"""

__author__ = 'Ken Benoit'

# Test messages
KEYBOARD_INTERRUPT = 'Interrupt captured.'
INVALID_TEST_STEP = 'Invalid test step encountered.'
INVALID_TEST_STEP_INDEX = 'Invalid test step index encountered.'
INVALID_TEST_STEP_TYPE = 'Invalid test step index type encountered.'
UNEXPECTED_EXCEPTION = 'Unexpected exception encountered.'

# Test result messages
TEST_RESULT_PASS = 'PASS'
TEST_RESULT_FAIL = 'FAIL'
TEST_RESULT_ABORT = 'ABORT'

