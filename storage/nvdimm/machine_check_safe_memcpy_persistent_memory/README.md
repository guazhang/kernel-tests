# storage/nvdimm/machine_check_safe_memcpy_persistent_memory

Storage: nvdimm feature test for machine check safe memcpy BZ1437205

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
